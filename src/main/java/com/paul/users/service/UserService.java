package com.paul.users.service;

import com.paul.users.model.UserDTO;

import java.util.List;

/**
 * Created by Paul on 1/27/2019.
 */
public interface UserService {

    UserDTO createUser(UserDTO userDTO);

    UserDTO getUserByNickname(String nickname);

    UserDTO getUserByEmail(String email);

    List<UserDTO> getAllUsers();

    List<UserDTO> getAllUsersByCountry(String country);

    void updateUser(UserDTO userDTO, String email);

    void deleteUser(String email);

}
