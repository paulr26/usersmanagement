package com.paul.users.service;

import com.paul.users.Toolkit.Converter;
import com.paul.users.exceptions.NotFoundException;
import com.paul.users.model.UserDTO;
import com.paul.users.repository.User;
import com.paul.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Paul on 1/27/2019.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        User user = userRepository.save(Converter.convertUserDTOtoUser(userDTO));

        return Converter.convertUserToUserDTO(user);
    }

    @Override
    public UserDTO getUserByNickname(String nickname) {
        User userByNickname = userRepository.findByNickname(nickname)
                .orElseThrow(() -> new NotFoundException(String.format("User with nickname %s not found", nickname)));

        return Converter.convertUserToUserDTO(userByNickname);
    }

    @Override
    public UserDTO getUserByEmail(String email) {
        User userByEmail = userRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException(String.format("User with email %s not found", email)));

        return Converter.convertUserToUserDTO(userByEmail);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(Converter::convertUserToUserDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> getAllUsersByCountry(String country) {
        List<User> usersList = userRepository.findByCountry(country)
                .orElseThrow(() -> new NotFoundException(String.format("No users with country %s found", country)));

        return usersList.stream()
                .map(Converter::convertUserToUserDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void updateUser(UserDTO userDTO, String oldEmail) {
        User existingUser = userRepository
                .findByEmail(oldEmail)
                .orElseThrow(() -> new NotFoundException(String.format("User with email %s not found", oldEmail)));

        String firstName = userDTO.getFirstName();
        if (firstName != null) {
            existingUser.setFirstName(firstName);
        }

        String lastName = userDTO.getLastName();
        if (lastName != null) {
            existingUser.setLastName(lastName);
        }

        String nickname = userDTO.getNickname();
        if (nickname != null) {
            existingUser.setNickname(nickname);
        }

        String password = userDTO.getPassword();
        if (password != null) {
            existingUser.setPassword(password);
        }

        String email = userDTO.getEmail();
        if (email != null) {
            existingUser.setEmail(email);
        }

        String country = userDTO.getCountry();
        if (country != null) {
            existingUser.setCountry(country);
        }

        userRepository.save(existingUser);
    }

    @Override
    public void deleteUser(String email) {
        userRepository.deleteByEmail(email);
    }
}
