package com.paul.users.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by Paul on 1/27/2019.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByNickname(String nickname);

    Optional<User> findByEmail(String email);

    Optional<List<User>> findByCountry(String country);

    @Transactional
    void deleteByEmail(String email);
}
