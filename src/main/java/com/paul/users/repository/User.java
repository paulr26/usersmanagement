package com.paul.users.repository;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Created by Paul on 1/27/2019.
 */
@Data
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String nickname;

    //TODO Should be persisted as a hash.
    @NotNull
    private String password;

    @NotNull
    private String email;

    @NotNull
    private String country;

    public User() {
    }
}
