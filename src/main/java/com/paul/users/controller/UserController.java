package com.paul.users.controller;

import com.paul.users.model.UserDTO;
import com.paul.users.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Paul on 1/27/2019.
 */
@RestController
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @ApiOperation(value = "This endpoint is used for creating a new user.")
    @RequestMapping(value = "/users/create", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO userDTO) {
        LOGGER.debug("Creating new user with email: {}", userDTO.getEmail());

        UserDTO createdUser = userService.createUser(userDTO);

        LOGGER.debug("Creating new user");

        return new ResponseEntity<>(createdUser, HttpStatus.OK);
    }

    @ApiOperation(value = "This endpoint is used for finding a user by its nickname.")
    @RequestMapping(value = "/users/byNickname/{nickname}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> getUserByNickname(@PathVariable("nickname") String nickname) {
        LOGGER.debug(String.format("Finding user with nickname %s", nickname));

        UserDTO userDTO = userService.getUserByNickname(nickname);

        LOGGER.debug(String.format("User with nickname %s was found successfully", nickname));

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "This endpoint is used for finding a user by its email.")
    @RequestMapping(value = "/users/byEmail/{email}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> getUserByEmail(@PathVariable("email") String email) {
        LOGGER.debug(String.format("Finding user with email %s", email), email);

        UserDTO userDTO = userService.getUserByEmail(email);

        LOGGER.debug(String.format("User with email %s was found successfully", email));

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "This endpoint is used for finding all the persisted users.")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        LOGGER.debug("Finding all users");

        List<UserDTO> allUsers = userService.getAllUsers();

        LOGGER.debug(String.format("Found %s users", allUsers.size()));
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }

    @ApiOperation(value = "This endpoint is used for finding all the persisted users for a given country.")
    @RequestMapping(value = "/users/byCountry/{country}", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllUsersByCountry(@PathVariable("country") String country) {
        LOGGER.debug(String.format("Finding all users with country %s", country));

        List<UserDTO> allUsersByCountry = userService.getAllUsersByCountry(country);

        LOGGER.debug(String.format("Found %s users with country %s", allUsersByCountry.size(), country));

        return new ResponseEntity<>(allUsersByCountry, HttpStatus.OK);
    }

    @ApiOperation(value = "This endpoint is used for updating an existing user.")
    @RequestMapping(value = "/users/updateUser/{email}", method = RequestMethod.POST)
    public ResponseEntity<String> updateUser(
            @RequestBody UserDTO userDTO,
            @PathVariable String email
    ) {
        LOGGER.debug(String.format("Updating user with email %s", email));

        userService.updateUser(userDTO, email);

        LOGGER.debug(String.format("User with email %s updated successfully", email));
        return new ResponseEntity<>(
                String.format("User with email %s successfully was updated successfully", userDTO.getEmail()),
                HttpStatus.OK
        );
    }

    @ApiOperation(value = "This endpoint is used for deleting a user.")
    @RequestMapping(value = "/users/removeUser/{email}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUser(@PathVariable("email") String email) {
        LOGGER.debug(String.format("Deleting user with email %s", email));

        userService.deleteUser(email);

        LOGGER.debug(String.format("User with email %s was deleted successfully", email));
        return new ResponseEntity<>(
                String.format("User with email %s was deleted successfully", email),
                HttpStatus.OK
        );
    }
}
