package com.paul.users.Toolkit;

import com.paul.users.model.UserDTO;
import com.paul.users.repository.User;

/**
 * Created by Paul on 1/29/2019.
 */
public class Converter {

    public static final User convertUserDTOtoUser(UserDTO userDTO) {
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setNickname(userDTO.getNickname());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setCountry(userDTO.getCountry());

        return user;
    }

    public static final UserDTO convertUserToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setNickname(user.getNickname());
        userDTO.setPassword(user.getPassword());
        userDTO.setEmail(user.getEmail());
        userDTO.setCountry(user.getCountry());

        return userDTO;
    }
}
