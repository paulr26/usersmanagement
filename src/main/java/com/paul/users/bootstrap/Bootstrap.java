package com.paul.users.bootstrap;

import com.paul.users.repository.User;
import com.paul.users.repository.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 1/27/2019.
 */
@Component
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private UserRepository userRepository;

    public Bootstrap(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        createDefaultData();
    }

    private void createDefaultData() {
        List<User> users = new ArrayList<>();
        String romaniaSuffix = "_RO";
        String germanySuffix = "_DE";
        String greatBritainSuffix = "_GB";

        for (int i = 0; i < 10; i++) {
            User user = new User();
            if (i < 3) {
                user.setFirstName("FirstName" + i + romaniaSuffix);
                user.setLastName("LastName" + i + romaniaSuffix);
                user.setNickname("user" + i + romaniaSuffix);
                user.setPassword("pass" + romaniaSuffix);
                user.setEmail(user.getNickname() + "@gmail.com");
                user.setCountry("Romania");
            } else if (i < 7) {
                user.setFirstName("FirstName" + i + germanySuffix);
                user.setLastName("LastName" + i + germanySuffix);
                user.setNickname("user" + i + germanySuffix);
                user.setPassword("pass" + germanySuffix);
                user.setEmail(user.getNickname() + "@gmail.com");
                user.setCountry("Germany");
            } else {
                user.setFirstName("FirstName" + i + greatBritainSuffix);
                user.setLastName("LastName" + i + greatBritainSuffix);
                user.setNickname("user" + i + greatBritainSuffix);
                user.setPassword("pass" + greatBritainSuffix);
                user.setEmail(user.getNickname() + "@gmail.com");
                user.setCountry("Great Britain");
            }

            users.add(user);
        }


        userRepository.saveAll(users);

//        User user2 = User.builder()
//                .firstName("X")
//                .lastName("Y")
//                .password("pass1")
//                .email("x@gmail.com")
//                .country("Ro")
//                .build();
//        userRepository.save(user1);
    }
}
