package com.paul.users.model;

import lombok.Data;

/**
 * Created by Paul on 1/29/2019.
 */
@Data
public class UserDTO {
    private String firstName;
    private String lastName;
    private String nickname;
    private String password;
    private String email;
    private String country;
}
