package com.paul.users;

import com.paul.users.Toolkit.Converter;
import com.paul.users.exceptions.NotFoundException;
import com.paul.users.model.UserDTO;
import com.paul.users.repository.User;
import com.paul.users.repository.UserRepository;
import com.paul.users.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by Paul on 1/29/2019.
 */
public class UserServiceTest {
    private static final String NICKNAME = "John";
    private static final String COUNTRY = "Germany";
    private static final String EMAIL = "john@mail.com";

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService = new UserServiceImpl();

    private static final User getUser() {
        User user = new User();
        user.setFirstName("FirstName");
        user.setLastName("LastName");
        user.setPassword("pass");
        user.setNickname(NICKNAME);
        user.setCountry(COUNTRY);
        user.setEmail(EMAIL);
        return user;
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createUser() {
        User user = new User();

        when(userRepository.save(any(User.class))).thenReturn(user);

        assertTrue(userService.createUser(new UserDTO()) != null);
    }

    @Test
    public void getAllUsers() {
        List<User> users = Arrays.asList(getUser(), new User(), new User());

        when(userRepository.findAll()).thenReturn(users);

        userRepository.deleteByEmail(getUser().getEmail());

        List<UserDTO> userDTOlist = userService.getAllUsers();

        assertEquals(3, userDTOlist.size());
    }

    @Test
    public void userFoundByNickname() {
        User user = getUser();

        when(userRepository.findByNickname(NICKNAME)).thenReturn(Optional.of(user));

        UserDTO userDTO = userService.getUserByNickname(NICKNAME);

        assertEquals(user.getFirstName(), userDTO.getFirstName());
        assertEquals(user.getLastName(), userDTO.getLastName());
        assertEquals(user.getPassword(), userDTO.getPassword());
        assertEquals(NICKNAME, userDTO.getNickname());
        assertEquals(COUNTRY, userDTO.getCountry());
        assertEquals(EMAIL, userDTO.getEmail());
    }

    @Test(expected = NotFoundException.class)
    public void userNotFoundByNickname() {
        User user = getUser();

        when(userRepository.findByNickname(NICKNAME)).thenReturn(Optional.of(user));

        userService.getUserByNickname("unknownNickname");
    }

    @Test
    public void userFoundByEmail() {
        User user = getUser();

        when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.of(user));

        UserDTO userDTO = userService.getUserByEmail(EMAIL);

        assertEquals(user.getFirstName(), userDTO.getFirstName());
        assertEquals(user.getLastName(), userDTO.getLastName());
        assertEquals(user.getPassword(), userDTO.getPassword());
        assertEquals(NICKNAME, userDTO.getNickname());
        assertEquals(COUNTRY, userDTO.getCountry());
        assertEquals(EMAIL, userDTO.getEmail());
    }

    @Test(expected = NotFoundException.class)
    public void userNotFoundByEmail() {
        User user = getUser();

        when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.of(user));

        userService.getUserByEmail("unknownEmail");
    }

    @Test
    public void usersFoundByCountry() {
        Optional<List<User>> userList = Optional.of(Arrays.asList(getUser()));

        when(userRepository.findByCountry(COUNTRY)).thenReturn(userList);

        List<UserDTO> userDTO = userService.getAllUsersByCountry(COUNTRY);

        assertEquals(userDTO.size(), 1);
        assertEquals(userDTO.get(0).getCountry(), COUNTRY);
    }

    @Test
    public void updateUser() {
        User user = getUser();
        UserDTO userDTO = Converter.convertUserToUserDTO(user);
        when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.of(user));
        userService.updateUser(userDTO, EMAIL);

        verify(userRepository, times(1)).findByEmail(EMAIL);
        verify(userRepository, times(1)).save(Converter.convertUserDTOtoUser(userDTO));
    }

    @Test
    public void deleteUser() {
        userService.deleteUser(EMAIL);

        verify(userRepository, times(1)).deleteByEmail(EMAIL);
    }
}
