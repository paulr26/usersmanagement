UserManagement microservice

Prerequisites:
- JDK 1.8 or later
- Maven 3.3+

Install and run the app:
- Open a terminal and type: mvn clean install. Wait until the message "BUILD SUCCESS" is displayed
- Type in the terminal: mvn spring-boot:run. 
If you can see the  message "Start UserApplication..." the application started successfuly.

- for seeing the available endpoints open any brower and go to: http://localhost:8080/swagger-ui.html#/
- the endpoints are listed under user-controller
- health check: http://localhost:8080/actuator/health
- access the h2 database: http://localhost:8080/h2-console and click connect
The database is populated when the microservice starts with 10 users.

Technologies used:
- JDK 1.8
- Intellij IDEA
- Maven - for building the project and managing dependencies
- Spring Boot - for rapid application development
- Other spring modules: -spring-boot-starter-web: -contains dependencies needed for creating a web project like spring mvc, spring-core, tomcat etc.
						- spring-data-jpa and h2 database: for persisting data
						- spring actuator: - for configuring teh health check endpoints
						- springfox-swagger2 && springfox-swagger-ui: for documenting the endpoints
- Postman - for testing the endpoints
- lombok - java library for eliminating boilerplate code


						






